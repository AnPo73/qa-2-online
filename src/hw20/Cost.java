package hw20;

public interface Cost {
    void returnCost(Buyer buyer, Seller seller, Product product);
}