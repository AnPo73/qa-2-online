package testing.hw28;

import hw26.Waiters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    static ChromeOptions options;
    static WebDriver driver;
    static Waiters waiters;

    public void initialDriver() {
        options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        driver = new ChromeDriver(options);
    }

    public void waiters() {
        waiters = new Waiters(driver);
    }

    public void setDriver() {
        driver.manage().window().maximize();
    }

    public void closeDriver() {
        driver.quit();
    }

    @BeforeClass
    public void startTest() {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        initialDriver();
        waiters();
        setDriver();
    }

    @AfterClass
    public void stopTest() {
        closeDriver();
    }
}