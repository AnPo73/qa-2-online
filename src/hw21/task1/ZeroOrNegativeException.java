package hw21.task1;

public class ZeroOrNegativeException extends Exception {
    public String getMessage() {
        return "\nZero or negative value...";
    }
}