package hw21.task3;

public class NotNumberException extends Exception{
    public String getMessage() {
        return "\nIt's not a number...";
    }
}
