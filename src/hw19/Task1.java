package hw19;

//        Создать класс Computer c конструктором принимающим параметры
//        Марка тип String, цена тип int, оперативная память тип int,
//        и видеокарта тип int.
//        Конструктор также выводит на консоль сообщение типа:
//        "Создан PC.
//        Имя = марка.
//        Цена = цена.
//        Видеокарта = объем видеокарты
//        ОЗУ = Объем оперативной памяти."
//        Все поля класса Computer должны быть приватными.
//        Также создайте публичные методы для получения информации о полях класса Computer.
//        А также методы для изменения его полей.
//        Создайте один метод для получения всей информации об объекте класса Computer.
//        В отдельном классе создайте объект класса компьютер, и выведите на экран его поля при использовании
//        одного созданного выше метода класса.

public class Task1 {
    public static void main(String[] args) {
        Computer computer = new Computer("IBM", 1000, 16, 2);
        System.out.println("\n" + computer);
        computer.getInfo();
    }
}

class Computer {
    private String brand;
    private int price;
    private int ram;
    private int videoCard;

    public Computer() {
    }

    public Computer(String brand, int price, int ram, int videoCard) {
        this.brand = brand;
        this.price = price;
        this.ram = ram;
        this.videoCard = videoCard;
        System.out.printf("%nСоздан PC:%nИмя = %1$s,%nЦена = %2$d,%nВидеокарта = %4$d,%nОЗУ = %3$d.%n", brand, price, ram, videoCard);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(int videoCard) {
        this.videoCard = videoCard;
    }

    public Computer getComputer(Computer computer) {
        return computer;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "brand='" + brand + '\'' +
                ", price=" + price +
                ", ram=" + ram +
                ", videoCard=" + videoCard +
                '}';
    }

    public void getInfo() {
        System.out.println("Информация о PC: Имя = " + getBrand() + ", Цена = " + getPrice() + ", ОЗУ = " + getRam() + ", Видеокарта = " + getVideoCard() + ".");
    }
}