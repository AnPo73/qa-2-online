package testing.hw27;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class Ex4Test extends BaseTest {
    private static class Url {
        private static final String guinnessWorldRecords = "https://www.guinnessworldrecords.com/Account/Login";
    }

    private static class Locators {
        private static final By rememberMe = By.cssSelector("#RememberMe");
    }

    @Test
    public void checkboxChecking() {
        driver.get(Url.guinnessWorldRecords);
        waiters.waitForPresenceOfElementLocated(Locators.rememberMe);
        assertFalse(driver.findElement(Locators.rememberMe).isSelected(),
                "The check box is in the wrong state");
    }

    @Test
    public void clickTheCheckbox() {
        driver.get(Url.guinnessWorldRecords);
        waiters.waitForPresenceOfElementLocated(Locators.rememberMe);
        driver.findElement(Locators.rememberMe).click();
        assertTrue(driver.findElement(Locators.rememberMe).isSelected(),
                "The check box is in the wrong state");
    }

    @Test
    public void doubleClickTheCheckbox() {
        driver.get(Url.guinnessWorldRecords);
        waiters.waitForPresenceOfElementLocated(Locators.rememberMe);
        driver.findElement(Locators.rememberMe).click();
        driver.findElement(Locators.rememberMe).click();
        assertFalse(driver.findElement(Locators.rememberMe).isSelected(),
                "The check box is in the wrong state");
    }
}