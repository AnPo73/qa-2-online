package hw17;

//        Заполнить массив на 45 элементов случайными числами
//        от -50 до +50. Найти минимальный элемент и вывести его
//        на консоль. Найти максимальный элемент и вывести его на
//        консоль.

import java.util.Arrays;

public class Task3 {

    static int[] array = new int[45];

    public static void main(String[] args) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) ((Math.random() * 101) - 50);
        }
//        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
//        System.out.println(Arrays.toString(array));
        System.out.println("Minimum array element = " + array[0]);
        System.out.println("Maximum array element = " + array[array.length - 1]);
    }
}