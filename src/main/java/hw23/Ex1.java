package hw23;

//        Написать программу, которая будет открывать пять различных страниц в новых окнах:
//        https://uhomki.com.ua/ru/koshki/1074/
//        https://zoo.kiev.ua/
//        https://www.w3schools.com/
//        https://taxi838.ua/ru/dnepr/
//        https://klopotenko.com/
//        Прописать цикл, который будет переключаться поочередно через все страницы,
//        для каждой страницы выводить в консоль название и ссылку на эту страницу.
//        И будет закрывать ту страницу в названии которой есть слово зоопарк.

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.Set;

public class Ex1 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/ru/koshki/1074/";
        private static final String zoo = "https://zoo.kiev.ua/";
        private static final String w3schools = "https://www.w3schools.com/";
        private static final String taxi838 = "https://taxi838.ua/ru/dnepr/";
        private static final String klopotenko = "https://klopotenko.com/";
    }

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        List<String> urls = List.of(Url.uhomki, Url.zoo, Url.w3schools, Url.taxi838, Url.klopotenko);

        for (int i = 0; i < urls.size(); i++) {
            driver.navigate().to(urls.get(i));
            System.out.println("\n" + driver.getCurrentUrl() + "\n" + driver.getTitle());
            Set<String> set1 = driver.getWindowHandles();
            String descriptor1 = set1.iterator().next();
            if (driver.getTitle().contains("зоопарк")) {
                driver.close();
                driver.switchTo().window(descriptor1);
            }
            if (i < urls.size() - 1) {
                ((JavascriptExecutor) driver).executeScript("window.open()");
                Set<String> set2 = driver.getWindowHandles();
                set2.removeAll(set1);
                String descriptor2 = set2.iterator().next();
                driver.switchTo().window(descriptor2);
            }
        }
        driver.quit();
    }
}