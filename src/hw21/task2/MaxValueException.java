package hw21.task2;

public class MaxValueException extends Exception{
    public String getMessage() {
        return "\nMaximum Integer value reached...";
    }
}