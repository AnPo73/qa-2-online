package hw23;

//        Написать метод, который выводит сообщение об айди элемента,
//        значении тега элемента, значении класса элемента,
//        значении атрибута name элемента, текста данного элемента,
//        а также о координатах центра контейнера данного элемента.
//        Создать свой тип исключений, который будет вызываться если у элемента
//        нет определенного атрибута и на экран будет выводиться сообщение об отсутствии данного атрибута.

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;

public class Ex3 {
    private static class Url {
        private static final String w3schools = "https://www.w3schools.com/";
        private static final String danIt = "https://dan-it.com.ua/";
    }

    private static class Locators {
        private static final By w3schoolsXpath = By.xpath("//a[@id = 'navbtn_tutorials']");
        private static final By danItCssSelector = By.cssSelector("[name = 'userName']");
    }

    public static void main(String[] args) {
        Ex3 ex3 = new Ex3();
        ex3.elementAttributes(Url.w3schools, Locators.w3schoolsXpath);
        ex3.elementAttributes(Url.danIt, Locators.danItCssSelector);
    }

    public void elementAttributes(String url, By locator) {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(url);
        WebElement element = driver.findElement(locator);

        String id = element.getAttribute("id");
        String tag = element.getTagName();
        String grade = element.getAttribute("class");
        String name = element.getAttribute("name");
        String text = element.getText();

        try {
            if (Objects.equals(id, "")) {
                throw new NoId();
            } else {
                System.out.println("id = " + element.getAttribute("id"));
            }
        } catch (NoId e) {
            System.err.println(e.getMessage());
        }

        try {
            if (Objects.equals(tag, "")) {
                throw new NoTag();
            } else {
                System.out.println("tag = " + element.getTagName());
            }
        } catch (NoTag e) {
            System.err.println(e.getMessage());
        }

        try {
            if (Objects.equals(grade, "")) {
                throw new NoClass();
            } else {
                System.out.println("class = " + element.getAttribute("class"));
            }
        } catch (NoClass e) {
            System.err.println(e.getMessage());
        }

        try {
            if (Objects.equals(name, "")) {
                throw new NoName();
            } else {
                System.out.println("name = " + element.getAttribute("name"));
            }
        } catch (NoName e) {
            System.err.println(e.getMessage());
        }

        try {
            if (Objects.equals(text, "")) {
                throw new NoText();
            } else {
                System.out.println("text = " + element.getText());
            }
        } catch (NoText e) {
            System.err.println(e.getMessage());
        }

        System.out.println("container center = (" + (element.getLocation().x + element.getSize().width / 2) +
                ", " + (element.getLocation().y + element.getSize().height / 2) + ")");
        driver.quit();
    }

    private static class NoId extends Exception {
        public String getMessage() {
            return "The element has no id...";
        }
    }

    private static class NoTag extends Exception {
        public String getMessage() {
            return "The element has no tag...";
        }
    }

    private static class NoClass extends Exception {
        public String getMessage() {
            return "The element has no class...";
        }
    }

    private static class NoName extends Exception {
        public String getMessage() {
            return "The element has no name...";
        }
    }

    private static class NoText extends Exception {
        public String getMessage() {
            return "The element has no text...";
        }
    }
}