package hw14;

//        Создать строку string = "Testing, is my favourite job"
//        Вывести на экран отдельно каждое слово и длину этого слова в след виде:
//        Cлово1 = (значение слова), Длина этого слова = (значение длины слова).
//        Cлово2 = (значение слова), Длина этого слова = (значение длины слова).
//        и т.д.
//        Вывести на консоль true, если первое слово длиннее остальных, в
//        противном случае вывести false.

public class Task4 {
    public static void main(String[] args) {
        String string = "Testing, is my favourite job";
        String testing = string.split(", ")[0];
        String is = string.split(" ")[1];
        String my = string.split(" ")[2];
        String favourite = string.split(" ")[3];
        String job = string.split(" ")[4];
        boolean comparison = testing.length() > is.length() &&
                testing.length() > my.length() &&
                testing.length() > favourite.length() &&
                testing.length() > job.length();

        System.out.println("\n" + testing + ", Длина этого слова = " + testing.length());
        System.out.println(is + ", Длина этого слова = " + is.length());
        System.out.println(my + ", Длина этого слова = " + my.length());
        System.out.println(favourite + ", Длина этого слова = " + favourite.length());
        System.out.println(job + ", Длина этого слова = " + job.length());
        System.out.println("\nПервое слово длиннее остальных: " + comparison);
    }
}
