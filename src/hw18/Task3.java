package hw18;

//        Используя рекурсию напишите метод, который будет выводить введенное число в обратном порядке, так чтобы каждая
//        цифра писалась два раза, после ставился пробел, и вторая цифра тоже писалась два раза и после ставился пробел...
//        Пример: вы вводите 563
//        вывод будет следующий 33 66 55

public class Task3 {
    public static void main(String[] args) {
        int range = 1001;
        int i = (int) (Math.random() * range);
        System.out.println("\ni = " + i);
        System.out.print("result = ");
        recursion(i);
    }

    static void recursion(int i) {
        if (!(i / 10 == 0)) {
            System.out.print(i % 10 + "" + i % 10 + " ");
            i = i / 10;
            recursion(i);
        } else {
            System.out.println(i % 10 + "" + i % 10);
        }
    }
}