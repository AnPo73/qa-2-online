package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex7 {
    private static class Url {
        private static final String w3schools = "https://www.w3schools.com/";
    }

    private static class Locators {
        private static final By locator1 = By.xpath("//a[@id = 'navbtn_tutorials']");
        private static final By locator2 = By.xpath("//a[@id = 'navbtn_references']");
        private static final By locator3 = By.xpath("//a[@id = 'navbtn_exercises']");
        private static final By locator4 = By.xpath("//a[@id = 'w3loginbtn']");
        private static final By locator5 = By.xpath("//a[@id = 'signupbtn_topnav']");
        private static final By locator6 = By.xpath("//a[@id = 'cert_navbtn']");
        private static final By locator7 = By.xpath("//a[@id = 'nav_search_btn']");
        private static final By locator8 = By.xpath("//a[@id = 'nav_translate_btn']");
        private static final By locator9 = By.xpath("//input[@id = 'search2']");
        private static final By locator10 = By.xpath("//button[@id = 'learntocode_searchbtn']");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.w3schools);
        WebElement hoverable = driver.findElement(Locators.locator1);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator2);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator3);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator4);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator5);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator6);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator7);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator8);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        driver.findElement(Locators.locator9).sendKeys("щось там тіпу шукаємо");
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator10);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
    }
}