package hw21.task3;

public class OutOfBoundsException extends Exception{
    public String getMessage() {
        return "\nIndex beyond array length...";
    }
}
