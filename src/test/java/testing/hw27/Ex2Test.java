package testing.hw27;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Ex2Test extends BaseTest {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/ua/";
    }

    private static class Locators {
        private static final By oplataDostavka = By.cssSelector("[href = '/ua/oplata-i-dostavka/']");
    }

    @Test
    public void pageWithDesiredLinkOpens() {
        driver.get(Url.uhomki);
        waiters.waitForElementToBeClickable(Locators.oplataDostavka);
        driver.findElement(Locators.oplataDostavka).click();
        assertEquals(driver.getCurrentUrl(), "https://uhomki.com.ua/ua/oplata-i-dostavka/", "Another page opens");
    }
}