package hw13;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        String string1 = "Казнить";
        String string2 = "нельзя";
        String string3 = "помиловать";
        String string4 = ",";
        String string5 = ".";

        System.out.println("\n" + string1 + string4 + " " + string2 + " " + string3 + string5);
        System.out.println(string1 + " " + string2 + string4 + " " + string3 + string5);

        System.out.println("\tAnother solution...");

        List<String> line1 = List.of(string1 + string4 + " " + string2 + " " + string3 + string5 + "\n");
        List<String> line2 = List.of(string1 + " " + string2 + string4 + " " + string3 + string5 + "\n");
        line1.forEach(System.out::print);
        line2.forEach(System.out::print);
    }
}