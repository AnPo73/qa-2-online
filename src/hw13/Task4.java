package hw13;

import java.util.Scanner;

public class Task4 {

    static Scanner console = new Scanner(System.in);
    final static int two = 2;

    public static void main(String[] args) {
        System.out.println("\n-=Четное ли это число?=-\n");
        System.out.print("Введите любое целое число: ");
        while (!console.hasNextInt()) {
            System.out.print("Недопустимое значение, повторите попытку: ");
            console.nextLine();
        }
        int number = console.nextInt();
        boolean chet = number % two == 0;
        String chetString = String.valueOf(chet);
        System.out.println("Число number имеет значение " + number + ".");
        System.out.println("Является ли оно четным?\n");
        System.err.println("-=" + chetString.toUpperCase() + "=-");
    }
}