package hw14;

//        Создать две переменные типа double:
//        chislitel = 7.0;
//        znamenatel =5.0;
//        Используя явный кастинг вывести на экран результат приведения 7.0/5.0 к виду правильной дроби.
//        То бишь вывести текст следующего типа:
//        Целая часть равна - (значение целой части без вещественной).
//        Остаток от деления равен - (значение остатка от деления, в виде целого числа, без вещественной части).
//        Знаменатель равен - (значения знаменателя в виде целой части, без вещественной части).

public class Task2 {
    public static void main(String[] args) {
        double chislitel = 7.0;
        double znamenatel = 5.0;

        int numeratorInt = (int) chislitel;
        int denominatorInt = (int) znamenatel;
        int integerPartInt = numeratorInt / denominatorInt;
        int remainderInt = numeratorInt % denominatorInt;

        System.out.println("Целая часть равна - " + integerPartInt + "." + "\n"
                + "Остаток от деления равен - " + remainderInt + "." + "\n"
                + "Знаменатель равен - " + denominatorInt + ".");
    }
}