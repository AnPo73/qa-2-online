package hw14;

//        Создать переменную string1 = "This line that i want to cut, cause it is too long"
//        Создать строку string2 в которой должно быть помещено значение строки string1
//        Обрезанное до "This line that i want to cut, cause".
//        Создать строку string3 на основе string2 которое будет содержать значение
//        "This line that don't want to cut, cause it is perfect".
//        Вывести на консоль каждое сообщение и его длину.

public class Task3 {
    public static void main(String[] args) {
        String string1 = "This line that i want to cut, cause it is too long";
        String string2 = string1.substring(0, 35);
        String stringTemp = string2.replace("i want", "don't want");
        String string3 = stringTemp.concat(" it is perfect");

        System.out.println(string1 + "\t\tlength: " + string1.length());
        System.out.println(string2 + "\t\t\t\t\t\tlength: " + string2.length());
        System.out.println(string3 + "\tlength: " + string3.length());
    }
}