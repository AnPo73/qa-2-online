package hw21.task4;

public enum Food {
    APPLE,
    POTATO,
    MILK,
    BEER,
    TOBACCO;
}