package hw19;

//        Необходимо создать класс конвертер, который будет иметь методы конвертации примитивных типов данных:
//        метод convertToInt; (конвертирует byte, short, int, long, char, float, double, String). При вводе boolean выводит сообщение, что введен тип boolean.
//        convertToDouble;    (конвертирует byte, short, int, long, char, float, double, String). При вводе boolean выводит сообщение, что введен тип boolean.
//        convertToString;     (конвертирует byte, short, int, long, char, boolean, float, double, String).
//        У данного класса должен быть только один конструктор, в параметрах которого можно указать его имя.
//        А также только один метод геттер для получения информации о названии данного конвертера.

public class Task3 {
    public static void main(String[] args) {
        Converter converter = new Converter("Large Hadron Converter");
        byte b = 5;
        short sh = 15;
        int i = 25;
        long l = 35;
        char c = '9';
        float f = 19;
        double d = 29;
        String s = "39";
        boolean bl = true;

        System.out.println("\nname = " + converter.getName() + ", byte = " + b + ", short = " + sh + ", int = " + i
                + ", long = " + l + "\nchar = " + c + ", float = " + f + ", double = " + d + ", String = " + s + ", boolean = " + bl + "\n");

        System.out.print("byte to int = " + converter.convertToInt(b));
        System.out.print(", short to int = " + converter.convertToInt(sh));
        System.out.print(", int to int = " + converter.convertToInt(i));
        System.out.print(", long to int = " + converter.convertToInt(l));
        System.out.println(", char to int = " + converter.convertToInt(c));
        System.out.print("float to int = " + converter.convertToInt(f));
        System.out.print(", double to int = " + converter.convertToInt(d));
        System.out.print(", String to int = " + converter.convertToInt(s));
        System.out.println(", boolean to int = " + converter.convertToInt(bl) + "\n");

        System.out.print("byte to double = " + converter.convertToDouble(b));
        System.out.print(", short to double = " + converter.convertToDouble(sh));
        System.out.print(", int to double = " + converter.convertToDouble(i));
        System.out.print(", long to double = " + converter.convertToDouble(l));
        System.out.println(", char to double = " + converter.convertToDouble(c));
        System.out.print("float to double = " + converter.convertToDouble(f));
        System.out.print(", double to double = " + converter.convertToDouble(d));
        System.out.print(", String to double = " + converter.convertToDouble(s));
        System.out.println(", boolean to double = " + converter.convertToDouble(bl) + "\n");

        System.out.print("byte to String = " + converter.convertToString(b));
        System.out.print(", short to String = " + converter.convertToString(sh));
        System.out.print(", int to String = " + converter.convertToString(i));
        System.out.print(", long to String = " + converter.convertToString(l));
        System.out.println(", char to String = " + converter.convertToString(c));
        System.out.print("float to String = " + converter.convertToString(f));
        System.out.print(", double to String = " + converter.convertToString(d));
        System.out.print(", String to String = " + converter.convertToString(s));
        System.out.println(", boolean to String = " + converter.convertToString(bl));
    }
}

class Converter {
    private String name;

    public Converter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int convertToInt(byte b) {
        return b;
    }

    public int convertToInt(short sh) {
        return sh;
    }

    public int convertToInt(int i) {
        return i;
    }

    public int convertToInt(long l) {
        return (int) l;
    }

    public int convertToInt(char c) {
        return c;
    }

    public int convertToInt(float f) {
        return (int) f;
    }

    public int convertToInt(double d) {
        return (int) d;
    }

    public int convertToInt(String s) {
        return Integer.parseInt(s);
    }

    public String convertToInt(boolean bl) {
        return "boolean type introduced";
    }

    public double convertToDouble(byte b) {
        return b;
    }

    public double convertToDouble(short sh) {
        return sh;
    }

    public double convertToDouble(int i) {
        return i;
    }

    public double convertToDouble(long l) {
        return (double) l;
    }

    public double convertToDouble(char c) {
        return c;
    }

    public double convertToDouble(float f) {
        return f;
    }

    public double convertToDouble(double d) {
        return d;
    }

    public double convertToDouble(String s) {
        return Double.parseDouble(s);
    }

    public String convertToDouble(boolean bl) {
        return "boolean type introduced";
    }

    public String convertToString(byte b) {
        return String.valueOf(b);
    }

    public String convertToString(short sh) {
        return String.valueOf(sh);
    }

    public String convertToString(int i) {
        return String.valueOf(i);
    }

    public String convertToString(long l) {
        return String.valueOf(l);
    }

    public String convertToString(char c) {
        return String.valueOf(c);
    }

    public String convertToString(float f) {
        return String.valueOf(f);
    }

    public String convertToString(double d) {
        return String.valueOf(d);
    }

    public String convertToString(String s) {
        return s;
    }

    public String convertToString(boolean bl) {
        return String.valueOf(bl);
    }
}