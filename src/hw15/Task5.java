package hw15;

//        Написать алгоритм, где у пользователя просят ввести
//        спецификатор формата для нынешнего времени либо даты.
//        И в зависимости от ввода этого спецификатора на консоль
//        будет выводиться дата либо время в указанном формате.

import java.util.Date;
import java.util.Scanner;

public class Task5 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        Date date = new Date();
        System.out.print("Enter a format specifier for the current time or date: ");

//        %1$tA %1$td %1$tB
//        %1$td:%1$tm:%1$ty
//        %1$tA %1$tB %1$tY
//        %1$tH:%1$tM:%1$tS:%1$tL:%1$tN %1$tp
//        %tT

        String s = console.nextLine();
        String formattedDate = String.format(s, date);
        System.out.println(formattedDate);
    }
}