package hw15;

//        Пользователь вводит с клавиатуры три целочисленных значения
//        На экран выводится информация можно ли из этих сторон
//        построить треугольник.

import java.util.Scanner;

public class Task2 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        int[] myArray = new int[3];
        for (int i = 0; i < 3; i++) {
            System.out.print("Enter any integer: ");
            while (!console.hasNextInt()) {
                System.out.print("Invalid value, please try again: ");
                console.nextLine();
            }
            myArray[i] = console.nextInt();
            console.nextLine();
        }
        boolean triangle = myArray[0] + myArray[1] > myArray[2] &&
                myArray[1] + myArray[2] > myArray[0] &&
                myArray[2] + myArray[0] > myArray[1];
        System.out.println("Can these sides form a triangle? The answer is " + triangle + ".");
    }
}