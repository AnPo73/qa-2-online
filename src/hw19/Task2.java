package hw19;

//        Создать класс Tree с перегруженными конструкторами.
//        В классе есть Tree есть поля:
//        String type; int height, int countOfSticks, String colour;
//        Конструктор1 принимающий в параметры String type; int height
//        и присваивающий значение полям countOfSticks значение 13,
//        colour "Зеленый".
//        Конструктор2 принимающий в параметры int height, int countOfSticks,
//        String colour и присваивающий значение полю type "пихта".
//        Конструктор по умолчанию 3, который присваивает height = 350, countOfSticks = 29,
//        colour = "Желтый".
//        Конструктор4 который принимает в параметры String type,
//        и вызывает внутри себя конструктор 3.
//        Создать в отдельном классе main 4 объекта класса Tree используя 4 созданных конструктора.

public class Task2 {
    public static void main(String[] args) {
        Tree tree1 = new Tree("Сосна", 10);
        Tree tree2 = new Tree(8, 9, "Изумрудный");
        Tree tree3 = new Tree();
        Tree tree4 = new Tree("Желтая сосна");
        System.out.println("\n" + tree1 + "\n" + tree2 + "\n" + tree3 + "\n" + tree4);
    }
}

class Tree {
    String type;
    int height;
    int countOfSticks;
    String colour;

    public Tree(String type, int height) {
        this.type = type;
        this.height = height;
        this.countOfSticks = 13;
        this.colour = "Зеленый";
    }

    public Tree(int height, int countOfSticks, String colour) {
        this.type = "Пихта";
        this.height = height;
        this.countOfSticks = countOfSticks;
        this.colour = colour;
    }

    public Tree() {
        this.height = 350;
        this.countOfSticks = 29;
        this.colour = "Желтый";
    }

    public Tree(String type) {
        this();
        this.type = type;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "type='" + type + '\'' +
                ", height=" + height +
                ", countOfSticks=" + countOfSticks +
                ", colour='" + colour + '\'' +
                '}';
    }
}
