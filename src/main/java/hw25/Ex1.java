package hw25;

import hw26.Waiters;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class Ex1 {
    private static class Url {
        private static final String google = "https://www.google.com/search";
        private static final String gwr = "https://www.guinnessworldrecords.com/account/register?";
        private static final String hyrTutorials = "https://www.hyrtutorials.com/p/alertsdemo.html";
        private static final String w3schools = "https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_submit";
    }

    private static class Locators {
        private static final By searchId = By.cssSelector("#APjFqb");
        private static final By CreateAccountGwr = By.xpath("//h3[text() = 'Create account | Guinness World Records']");
        private static final By AlertsDemoHyrTutorials = By.xpath("//h3[text() = 'AlertsDemo - H Y R Tutorials']");
        private static final By iframeResultW3schools = By.cssSelector("#iframeResult");
        private static final By fnameW3schools = By.cssSelector("#fname");
        private static final By lnameW3schools = By.cssSelector("#lname");
        private static final By submitW3schools = By.cssSelector("[value = 'Submit']");
        private static final By noteW3schools = By.cssSelector(".w3-panel.w3-pale-yellow.w3-leftbar.w3-border-yellow");
        private static final By lastNameGwr = By.cssSelector("#LastName");
        private static final By firstNameGwr = By.cssSelector("#FirstName");
        private static final By birthDayGwr = By.cssSelector("#DateOfBirthDay");
        private static final By birthMonthGwr = By.cssSelector("#DateOfBirthMonth");
        private static final By birthYearGwr = By.cssSelector("#DateOfBirthYear");
        private static final By countryGwr = By.cssSelector("#Country");
        private static final By stateGwr = By.xpath("//input[@id = 'State']");
        private static final By emailGwr = By.cssSelector("#EmailAddress");
        private static final By confirmEmailGwr = By.cssSelector("#ConfirmEmailAddress");
        private static final By passwordGwr = By.cssSelector("#Password");
        private static final By confirmPasswordGwr = By.cssSelector("#ConfirmPassword");
        private static final By fieldValidationGwr = By.cssSelector(".field-validation-error");
        private static final By alertBoxHyrTutorials = By.cssSelector("#alertBox");
        private static final By confirmBoxHyrTutorials = By.cssSelector("#confirmBox");
        private static final By promptBoxHyrTutorials = By.cssSelector("#promptBox");
        private static final By outputHyrTutorials = By.cssSelector("#output");
    }

    private static class Registration {
        private static final String surname = "Pohanovskyi";
        private static final String name = "Andrii";
        private static final String birthDay = "09";
        private static final String birthMonth = "08";
        private static final String birthYear = "1973";
        private static final String country = "UA";
        private static final String city = "Kyiv";
        private static final String email = "poganovsky@gmail.com";
        private static final String password1 = "111111";
        private static final String password2 = "222222";
        private static final String message = "'Final step of this task'";
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        Actions actions = new Actions(driver);
        Ex1 ex1 = new Ex1();
        Waiters waiters = new Waiters(driver);

        options.addArguments("--disable-notifications");
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(15));
        driver.get(Url.google);
        Set<String> set1 = driver.getWindowHandles();
        String firstDes = set1.iterator().next();

        ex1.googleSearch(driver, wait, Locators.searchId, Url.gwr);
        String secondDes = ex1.clickOnElementAndGetWindowHandler(driver, wait, actions, Locators.CreateAccountGwr);

        ex1.googleSearch(driver, wait, Locators.searchId, Url.hyrTutorials);
        String thirdDes = ex1.clickOnElementAndGetWindowHandler(driver, wait, actions, Locators.AlertsDemoHyrTutorials);

        driver.get(Url.w3schools);
        wait.until(ExpectedConditions.presenceOfElementLocated(Locators.iframeResultW3schools));
        WebElement iframeResultW3schools = driver.findElement(Locators.iframeResultW3schools);
        driver.switchTo().frame(iframeResultW3schools);
        WebElement fnameW3schools = driver.findElement(Locators.fnameW3schools);
        fnameW3schools.clear();
        fnameW3schools.sendKeys(Registration.name);
        WebElement lnameW3schools = driver.findElement(Locators.lnameW3schools);
        lnameW3schools.clear();
        lnameW3schools.sendKeys(Registration.surname);
        driver.findElement(Locators.submitW3schools).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(Locators.noteW3schools));
        WebElement noteW3schools = driver.findElement(Locators.noteW3schools);
        System.out.println(noteW3schools.getText().trim());

        driver.switchTo().window(secondDes);
        ex1.sendRegistrationData(driver, wait, Locators.lastNameGwr, Registration.surname);
        ex1.sendRegistrationData(driver, wait, Locators.firstNameGwr, Registration.name);
        ex1.sendRegistrationData(driver, wait, Locators.birthDayGwr, Registration.birthDay);
        ex1.sendRegistrationData(driver, wait, Locators.birthMonthGwr, Registration.birthMonth);
        ex1.sendRegistrationData(driver, wait, Locators.birthYearGwr, Registration.birthYear);
        ex1.sendRegistrationData(driver, wait, Locators.birthYearGwr, Registration.birthYear);
        ex1.selectBySelect(driver, wait, Locators.countryGwr, Registration.country);
        ex1.sendRegistrationData(driver, wait, Locators.stateGwr, Registration.city);
        ex1.sendRegistrationData(driver, wait, Locators.emailGwr, Registration.email);
        ex1.sendRegistrationData(driver, wait, Locators.confirmEmailGwr, Registration.email);
        ex1.sendRegistrationData(driver, wait, Locators.passwordGwr, Registration.password1);
        ex1.sendRegistrationData(driver, wait, Locators.confirmPasswordGwr, Registration.password2);
        ex1.sendEnter(driver, wait, Locators.confirmPasswordGwr);
        WebElement fieldValidationGwr = driver.findElement(Locators.fieldValidationGwr);
        System.out.println(fieldValidationGwr.getText().trim());

        driver.switchTo().window(thirdDes);
        waiters.waitSomeSeconds(2);
        WebElement output = driver.findElement(Locators.outputHyrTutorials);
        driver.findElement(Locators.alertBoxHyrTutorials).click();
        waiters.waitSomeSeconds(2);
        Alert alertBox = driver.switchTo().alert();
        alertBox.accept();
        System.out.println(output.getText());
        driver.findElement(Locators.confirmBoxHyrTutorials).click();
        waiters.waitSomeSeconds(2);
        Alert confirmBox = driver.switchTo().alert();
        confirmBox.dismiss();
        System.out.println(output.getText());
        driver.findElement(Locators.promptBoxHyrTutorials).click();
        waiters.waitSomeSeconds(2);
        Alert promptBox = driver.switchTo().alert();
        promptBox.sendKeys(Registration.message);
        promptBox.accept();
        System.out.println(output.getText());
        waiters.waitSomeSeconds(2);

        driver.quit();
    }

    public void googleSearch(WebDriver driver, WebDriverWait wait, By locator, String url) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(url);
        element.sendKeys(Keys.ENTER);
    }

    public String clickOnElementAndGetWindowHandler(WebDriver driver, WebDriverWait wait, Actions actions, By locator) {
        Set<String> set1 = driver.getWindowHandles();
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build().perform();
        Set<String> set2 = driver.getWindowHandles();
        set2.removeAll(set1);
        return set2.iterator().next();
    }

    public void sendRegistrationData(WebDriver driver, WebDriverWait wait, By locator, String registration) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        element.clear();
        element.sendKeys(registration);
    }

    public void sendEnter(WebDriver driver, WebDriverWait wait, By locator) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        element.sendKeys(Keys.ENTER);
    }

    public void selectBySelect(WebDriver driver, WebDriverWait wait, By locator, String registration) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        Select select = new Select(element);
        element.click();
        select.selectByValue(registration);
    }
}