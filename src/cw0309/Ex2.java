package cw0309;

//        пользователь вводит с клавиатуры два числа и символ
//        - или + или % или / или *
//        На экран выводится результат действия над двумя введенными числами.
//        Если пользователь ввел что-то кроме данных символов, то необходимо вывести 0.

import java.util.Scanner;

public class Ex2 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите два числа и действие: ");
        int number = console.nextInt();
        int number2 = console.nextInt();
        String string = console.nextLine().trim();
        System.out.println(string.equals("-") ? number - number2 :
                string.equals("+") ? number + number2 :
                        string.equals("%") ? number % number2 :
                                string.equals("/") ? number / number2 :
                                        string.equals("*") ? number * number2 : "0");
    }
}