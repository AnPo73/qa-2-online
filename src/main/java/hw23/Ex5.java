package hw23;

//Написать программу, которая повторит действия на видео "Задание 5.mp4".

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex5 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/ua/";
    }

    private static class Locators {
        private static final By searchInput = By.cssSelector(".search__input");
        private static final By catalogCard = By.cssSelector(".catalogCard-main-b");
        private static final By iconComparison = By.cssSelector(".icon.icon--comparison-s");
        private static final By comparisonView = By.cssSelector(".comparison-view");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(Url.uhomki);
        WebElement searchInput1 = driver.findElement(Locators.searchInput);
        searchInput1.sendKeys("тхір");
        Thread.sleep(2000);
        searchInput1.sendKeys(Keys.ENTER);
        driver.findElement(Locators.catalogCard).click();
        driver.findElement(Locators.iconComparison).click();
        WebElement searchInput2 = driver.findElement(Locators.searchInput);
        searchInput2.sendKeys("ведмідь");
        Thread.sleep(2000);
        searchInput2.sendKeys(Keys.ENTER);
        driver.findElement(Locators.catalogCard).click();
        driver.findElement(Locators.iconComparison).click();
        Thread.sleep(2000);
        driver.findElement(Locators.comparisonView).click();
        Thread.sleep(5000);
        driver.quit();
    }
}