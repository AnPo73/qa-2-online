package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex1 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/koshki/1074/";
    }

    private static class Locators {
        private static final By locator1 = By.linkText("Вход");
        private static final By locator2 = By.className("userbar__button-text");
        private static final By locator3 = By.xpath("//span[text() = 'Вход']");
        private static final By locator4 = By.xpath("//span[@class = 'userbar__button-text']");
        private static final By popupClose = By.xpath("//*[@id = 'sign-in']/div/a");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.uhomki);
        driver.findElement(Locators.locator1).click();
        Thread.sleep(1000);
        driver.findElement(Locators.popupClose).click();
        driver.findElement(Locators.locator2).click();
        Thread.sleep(1000);
        driver.findElement(Locators.popupClose).click();
        driver.findElement(Locators.locator3).click();
        Thread.sleep(1000);
        driver.findElement(Locators.popupClose).click();
        driver.findElement(Locators.locator4).click();
    }
}