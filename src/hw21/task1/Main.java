package hw21.task1;

//        Создайте метод, который просит у пользователя ввести с клавиатуры число
//        и делит данное число на другое случайно сгенерированное число в диапазоне от -10 до 10.
//        Исключительная ситуация должна возникать, если сгенерированное число равно нулю,
//        а также если полученное частное двух чисел принимает отрицательное значение.
//        Если сгенерированное число не равно нулю, то в любом случае данный метод
//        выводит в консоль информацию о том какое число было введено пользователем, какое число было
//        сгенерировано, и какое число получится при делении одного числа на другое.
//        Все исключительные ситуации обработать внутри метода.

import java.util.Scanner;

public class Main {
    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        Main main = new Main();
        try {
            main.numbersDivision();
        } catch (ZeroOrNegativeException e) {
            System.err.println(e.getMessage());
        }
    }

    public void numbersDivision() throws ZeroOrNegativeException {
        System.out.print("\nPlease enter the number: ");
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
        double consoleD = console.nextDouble();
        console.nextLine();
        double randomD = (Math.random() * 21) - 10;
//        double randomD = 0;
//        double randomD = -10;
        double divisionD = consoleD / randomD;
        if (randomD == 0) {
            throw new ZeroOrNegativeException();
        } else if (divisionD < 0) {
            System.out.printf("%nThe number entered by the user = %1$f%n" +
                    "The number which generated = %2$f%n" +
                    "The result of dividing these two numbers = %3$f%n", consoleD, randomD, divisionD);
            throw new ZeroOrNegativeException();
        } else {
            System.out.printf("%nThe number entered by the user = %1$f%n" +
                    "The number which generated = %2$f%n" +
                    "The result of dividing these two numbers = %3$f%n", consoleD, randomD, divisionD);
        }
    }
}