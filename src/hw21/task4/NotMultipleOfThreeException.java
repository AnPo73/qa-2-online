package hw21.task4;

public class NotMultipleOfThreeException extends Exception{
    public String getMessage() {
        return "\nQty of goods not a multiple of three...";
    }
}
