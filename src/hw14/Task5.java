package hw14;

//        Зная из урока что при применении к массиву строк метода length, можно узнать сколько
//        элементов находится в массиве. Попробовать вывести на экран какое количество раз
//        символ 'a' встречается в данной строке:
//        Completely random text in English. In it, we just need to determine how many times the character 'a' occurs there. And we can use the split method and the length method.
//        Решить данную задачу с использованием метода split,
//        а также с учетом верхнего и нижнего регистра символа 'a'.

public class Task5 {
    public static void main(String[] args) {

//        Забиваємо цвяхи мікроскопом

        String string = "Completely random text in English. In it, we just need to determine how many times the character 'a' occurs there. And we can use the split method and the length method.";
        String stringToUpperCase = string.toUpperCase();
        String a = "A";

        String[] stringSplit = stringToUpperCase.split(a);
        String stringJoin = String.join("", stringSplit);

        int stringLength = string.length();
        int stringJoinLength = stringJoin.length();

        System.out.println("Символ 'a' встречается в данной строке " + (stringLength - stringJoinLength)
                + " раз, с учетом верхнего и нижнего регистра символа 'a'.");

//        Гарне рішення

        String stringReplaceAll = stringToUpperCase.replaceAll(a, "");
        int stringReplaceAllLength = stringReplaceAll.length();
        System.out.println("Символ 'a' встречается в данной строке " + (stringLength - stringReplaceAllLength)
                + " раз, с учетом верхнего и нижнего регистра символа 'a'.");

//        Найгарніше рішення

        char c = 'A';
        System.out.println("Символ 'a' встречается в данной строке " + count(stringToUpperCase, c)
                + " раз, с учетом верхнего и нижнего регистра символа 'a'.");
    }

    private static long count(String s, char c) {
        return s.chars().filter(x -> x == c).count();
    }
}