package testing.hw27;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class Ex3Test extends BaseTest {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/ua/";
    }

    private static class Locators {
        private static final By goodsSearch = By.cssSelector("[placeholder = 'пошук товарів']");
        private static final By searchResultMessage = By.cssSelector("#j-catalog-header");
    }

    @Test(dataProvider = "searchProvider")
    public void SearchQueryIsPresentInSearchResultsMessage(String input) {
        driver.get(Url.uhomki);
        waiters.waitForPresenceOfElementLocated(Locators.goodsSearch);
        WebElement element = driver.findElement(Locators.goodsSearch);
        element.clear();
        element.sendKeys(input);
        element.submit();
        waiters.waitForPresenceOfElementLocated(Locators.searchResultMessage);
        assertTrue(driver.findElement(Locators.searchResultMessage).getText().contains(input),
                "The required word is not found in the search results message");
    }

    @DataProvider(name = "searchProvider")
    public Object[][] search() {
        return new Object[][]{{"смесь"}, {"спесь"}, {"ересь"}};
    }
}