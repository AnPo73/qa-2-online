package hw17;

//        Необходимо вывести на экран перевернутый прямоугольный треугольник прямым углом сверху в левой части.
//        Для вывода использовать условные операторы, циклы.
//        Команду System.out.println(); System.out.print(); и можно использовать только с одним символом *.
//        Пользователь вводит с клавиатуры число, которое будет считаться основанием треугольника,
//        а программа выводит сам треугольник.
//        Пример с числом 7:
//        *******
//        *    *
//        *   *
//        *  *
//        * *
//        *
//        Необходимо написать программу для вывода вначале заполненного, а потом и пустого треугольника.

import java.util.Scanner;

public class Task5 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("\nEnter triangle's base: ");
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
        int n = console.nextInt();
        console.nextLine();
        System.out.println();

        StringBuilder result1 = new StringBuilder();
        for (int i = n; i >= 1; i--) {
            result1.append("*".repeat(i));
            result1.append("\n");
        }
        System.out.println(result1);

        StringBuilder result2 = new StringBuilder();
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                if (i == n || j == 1 || j == i) {
                    result2.append("*");
                } else {
                    result2.append(" ");
                }
            }
            result2.append("\n");
        }
        System.out.print(result2);
    }
}