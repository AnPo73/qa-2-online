package hw17;

//        Создать три массива.
//        Первый будет состоять из следующих имен: "Петя", "Маша", "Алёна", "Федя", "Саша", "Антон", "Глеб".
//        Второй будет содержать следующие значения типа int: 10, 12, 14, 16, 18, 20.
//        Третий будет содержать следующие значения: "школу", "магазин", "церковь", "тренажерный зал", "кино", "поликлинику".
//        Пользователь вводит три числа с клавиатуры, которые будут соответствовать индексам каждого из элементов массивов.
//        Пример1. После ввода 3,2,1 на экране должно вывестись следующее сообщение:
//        "Федя будет идти в магазин в 14:00"
//        Пример2. После ввода 1,2,3 на экране должно вывестись следующее сообщение:
//        "Маша будет идти в тренажерный зал в 14:00"

import java.util.Scanner;

public class Task1 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        String[] names = {"Петя", "Маша", "Алёна", "Федя", "Саша", "Антон", "Глеб" };
        int[] numbers = {10, 12, 14, 16, 18, 20};
        String[] locations = {"школу", "магазин", "церковь", "тренажерный зал", "кино", "поликлинику" };

        System.out.print("\nChoose one of the values:\n\t0. Петя\n\t1. Маша\n\t2. Алёна" +
                "\n\t3. Федя\n\t4. Саша\n\t5. Антон\n\t6. Глеб\nEnter a number from 0 to 6: ");
        notNumber();
        int name = outOfRange(names.length - 1);

        System.out.print("\nChoose one of the values:\n\t0. 10\n\t1. 12\n\t2. 14" +
                "\n\t3. 16\n\t4. 18\n\t5. 20\nEnter a number from 0 to 5: ");
        notNumber();
        int number = outOfRange(numbers.length - 1);

        System.out.print("\nChoose one of the values:\n\t0. школу\n\t1. магазин\n\t2. церковь" +
                "\n\t3. тренажерный зал\n\t4. кино\n\t5. поликлинику\nEnter a number from 0 to 5: ");
        notNumber();
        int location = outOfRange(locations.length - 1);

        System.out.printf("%n%1$s будет идти в %3$s в %2$d:00%n", names[name], numbers[number], locations[location]);
    }

    public static void notNumber() {
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
    }

    public static int outOfRange(int i) {
        int var = console.nextInt();
        console.nextLine();
        while (var < 0 || var > i) {
            System.out.print("Invalid value, please try again: ");
            notNumber();
            var = console.nextInt();
            console.nextLine();
        }
        return var;
    }
}