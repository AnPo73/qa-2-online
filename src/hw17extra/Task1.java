package hw17extra;

//        Необходимо вывести на экран ромб при помощи символа #.
//        Для вывода использовать условные операторы, циклы.
//        Команду System.out.println(); и System.out.print(); можно использовать только
//        с одним символом #.
//        Пользователь вводит с клавиатуры число, которое будет считаться
//        высотой фигуры, а программа выводит pомб.
//        Пример с числом 7:
//           #
//          # #
//         #   #
//        #     #
//         #   #
//          # #
//           #

import java.util.Scanner;

public class Task1 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("\nEnter the height of rhombus: ");
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
        int n = console.nextInt() - 4;
        console.nextLine();
        System.out.println();

        for (int i = -n; i <= n; i++) {
            for (int j = -n; j <= n; j++) {
                int rhombus = Math.abs(i) + Math.abs(j);
                if (rhombus == n) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}