package hw15;

//        У вас есть строка "Я тестирую замечательно. Что еще нужно?",
//        Которую нужно ввести с клавиатуры в консоль.
//        Необходимо при помощи только метода next() класса Scanner
//        (не используем метод nextLine())
//        присвоить переменным типа String следующие значения:
//        string1 = Я
//        string2 = тестирую
//        string3 = замечательно
//        string4 = Что еще нужно?
//        Помимо string1, string2, string3, string4 новых переменных
//        создавать нельзя.

import java.util.Scanner;

public class Task1 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Please enter the text: ");
        String string1 = console.next();
        String string2 = console.next();
        String string3 = console.next() + "\b";
        String string4 = console.next();
        string4 = string4 + " " + console.next();
        string4 = string4 + " " + console.next();
        System.out.println(string1 + "\n" + string2 + "\n" + string3 + "\n" + string4);
    }
}