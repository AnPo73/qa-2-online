package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex2 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/koshki/1074/";
    }

    private static class Locators {
        private static final By locator1 = By.xpath("//div[@class = 'footer__heading']" +
                "/../ul/li[3]");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.uhomki);
        Thread.sleep(1000);
        driver.findElement(Locators.locator1).click();
    }
}