package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex9and10 {
    private static class Url {
        private static final String jetbrains = "https://www.jetbrains.com/idea//";
    }

    private static class Locators {
        private static final By locator1 = By.xpath("//a[@href = '/idea/download/']");
        private static final By locator2 = By.xpath("//a[@href = '/idea/download/download-thanks.html?platform=windows']");
        private static final By locator3 = By.xpath("//a[@href = '/idea/download/download-thanks.html?platform=windows&code=IIC']");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.jetbrains);
        driver.findElement(Locators.locator1).click();
        WebElement hoverable = driver.findElement(Locators.locator2);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        hoverable = driver.findElement(Locators.locator3);
        new Actions(driver).moveToElement(hoverable).perform();
    }
}