package hw16;

//        Написать программу, которая будет считывать введенные пользователем
//        слова с клавиатуры слова, и склеивать их в одно предложение до тех пор,
//        пока пользователь не введет с клавиатуры слово STOP.
//        Все слова введенные до этого должны отобразится в консоли
//        одним предложением.

import java.util.Scanner;

public class Task4 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        String s1 = "";
        StringBuilder s2 = new StringBuilder();
        do {
            System.out.print("Type in a word or \"STOP\" to exit: ");
            s1 = console.nextLine().trim();
            if (s1.equals("STOP") && s2.toString().isEmpty()) {
                s2 = new StringBuilder("Completion of the program...");
                break;
            } else if (s1.equals("STOP")) {
                break;
            } else if (s2.toString().isEmpty()) {
                s2 = new StringBuilder(s1);
            } else {
                s2.append(" ").append(s1);
            }
        } while (true);
        System.out.println(s2);
    }
}