package hw15;

//        Пользователь вводит с клавиатуры свой год рождения.
//        На консоль выводится информация о его возрасте.
//        (P.s пока не затрагиваем момент месяца рождения,
//        останавливаемся только на разнице годов. Если вам
//        вдруг понадобится преобразовать тип String в int,
//        то можете воспользоваться такой конструкцией Integer.parseInt(строка для преобразования).

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Task3 {

    static Scanner console = new Scanner(System.in);
    static Date currentDate = new Date();

    public static void main(String[] args) throws ParseException {
        System.out.print("Enter your year of birth (yyyy): ");
        String tempBirthDate = console.nextLine();
        Date birthDate = new SimpleDateFormat("yyyy").parse(tempBirthDate);
        int currentAge = currentDate.getYear() - birthDate.getYear();
        System.out.println("Your age is " + currentAge);
    }
}