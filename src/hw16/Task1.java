package hw16;

//        Используя оператор switch написать программу, которая выводит на
//        консоль ссылку для скачивания программы.
//        Из выбора программ взять: IntelliJ IDEA, Git, Java.
//        Из выбора ОС взять: Linux, macOS, Windows.
//        Программа должна спросить пользователя какая программа ему интересна,
//        также спросить какую ОС он использует, а после вывести в консоль необходимую ссылку.
//        Если программа с таким названием не существует выводить сообщение в консоль, о том
//        что такой программы не существует.
//        Если указанной пользователем ОС нет, то выводится сообщение в консоль, о том
//        что такой ОС не существует.

import java.util.Scanner;

public class Task1 {

//        IntelliJ IDEA
//        Git
//        Java
//        Linux
//        macOS
//        Windows

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Enter program or type \"end\" to exit: ");
        String s1 = console.nextLine().toLowerCase().trim();

        switch (s1) {
            case "intellij idea" -> {
                System.out.print("Enter OS or type \"end\" to exit: ");
                String s2 = console.nextLine().toLowerCase().trim();
                switch (s2) {
                    case "linux" -> System.out.println("Link to \"IntelliJ IDEA\" for \"Linux\"");
                    case "macos" -> System.out.println("Link to \"IntelliJ IDEA\" for \"macOS\"");
                    case "windows" -> System.out.println("Link to \"IntelliJ IDEA\" for \"Windows\"");
                    case "end" -> System.exit(0);
                    default -> System.err.println("No such OS exists...");
                }
            }
            case "git" -> {
                System.out.print("Enter OS or type \"end\" to exit: ");
                String s3 = console.nextLine().toLowerCase().trim();
                switch (s3) {
                    case "linux" -> System.out.println("Link to \"Git\" for \"Linux\"");
                    case "macos" -> System.out.println("Link to \"Git\" for \"macOS\"");
                    case "windows" -> System.out.println("Link to \"Git\" for \"Windows\"");
                    case "end" -> System.exit(0);
                    default -> System.err.println("No such OS exists...");
                }
            }
            case "java" -> {
                System.out.print("Enter OS or type \"end\" to exit: ");
                String s4 = console.nextLine().toLowerCase().trim();
                switch (s4) {
                    case "linux" -> System.out.println("Link to \"Java\" for \"Linux\"");
                    case "macos" -> System.out.println("Link to \"Java\" for \"macOS\"");
                    case "windows" -> System.out.println("Link to \"Java\" for \"Windows\"");
                    case "end" -> System.exit(0);
                    default -> System.err.println("No such OS exists...");
                }
            }
            case "end" -> System.exit(0);
            default -> System.err.println("No such program exists...");
        }
    }
}