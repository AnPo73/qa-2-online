package cw0305;

//        Написать программу в которой у нас имеется две строки string1 и string2.
//        Значение string1 = I am testing like a god
//        Значение string2 = the best of the best.
//        Получить символ 'e' из строки1 по индексу.
//        Вывести на экран coобщение в следующем виде, сохраня все переходы:
//        "Строка "the best of the best" содержит в себе символ 'e' и это (true или false).
//        Символ 'e' находится в строке номер один под номером (индекс).

public class Ex3 {
    public static void main(String[] args) {
        String string1 = "I am testing like a god";
        String string2 = "the best of the best";
        int index = string1.indexOf('e');
        char ch = string1.charAt(6);
        boolean result = string1.contains("e");
        System.out.println("Строка " + string2 + " содержит в себе символ " + ch + " и это " + result);
        System.out.println("Символ " + ch + " находится в строке номер один под номером " + index);
    }
}
