package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex5 {
    private static class Url {
        private static final String danIt = "https://dan-it.com.ua/";
    }

    private static class Locators {
        private static final By languageSelection = By.xpath("//a[text() = 'Українська']");
        private static final By locator1 = By.xpath("//a[@href = 'tel:0 800 335 695']");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.danIt);
        driver.findElement(Locators.languageSelection).click();
        driver.findElement(Locators.locator1).click();
    }
}