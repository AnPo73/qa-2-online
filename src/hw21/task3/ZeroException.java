package hw21.task3;

public class ZeroException extends Exception {
    public String getMessage() {
        return "\nZero value...";
    }
}