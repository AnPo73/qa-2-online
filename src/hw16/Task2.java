package hw16;

//        При помощи цикла while выполнить реализацию следующего алгоритма.
//        Пользователь вводит строку с клавиатуры. Программа должна вывести на
//        экран сколько символов "a", присутствует в данной строке.

import java.util.Scanner;

public class Task2 {

    static Scanner console = new Scanner(System.in);

//    The condition does not say what language the string is entered in.
//    Therefore, the program calculates the symbol "a" in both Latin and Cyrillic.

    public static void main(String[] args) {
        int i = 0;
        int counter = 0;
        System.out.print("Please enter the line: ");
        String s = console.nextLine().trim();
        char[] temp = s.toCharArray();
        while (i < s.length()) {
            if (temp[i] == 'a') counter++; //Latin
            if (temp[i] == 'а') counter++; //Cyrillic
            i++;
        }
        System.out.println("There are " + counter + " 'a' characters in this string");
    }
}