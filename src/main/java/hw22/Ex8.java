package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex8 {
    private static class Url {
        private static final String danIt = "https://dan-it.com.ua/";
    }

    private static class Locators {
        private static final By languageSelection = By.xpath("//a[text() = 'Українська']");
        private static final By locator1 = By.xpath("//li[@class = 'main-info__item']/../../div/ul/li[3]");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.danIt);
        driver.findElement(Locators.languageSelection).click();
        WebElement hoverable = driver.findElement(Locators.locator1);
        new Actions(driver).moveToElement(hoverable).perform();
    }
}