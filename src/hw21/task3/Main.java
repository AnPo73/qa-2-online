package hw21.task3;

//        Создать метод, в котором создается одномерный массив типа int произвольного размера
//        от 1 до 30, в котором каждому элементу массива присваивается произвольное значение от
//        0 до 30.
//        Данный метод спрашивает у пользователя ввести с клавиатуры индекс случайно сгенерированного массива.
//        Метод возвращает значение типа double, которое получается при делении элемента с указанным
//        индексом пользователя на первый элемент данного массива.
//        Продумать все возможные исключительные ситуации, которые могут возникнуть в данном методе.
//        Для каждой исключительной ситуации создать собственный класс checked исключения.
//        Прописать в методе условия генерации данных исключений, а также указать их в сигнатуре данного
//        метода.
//        Обработку исключения осуществить в стороннем классе Main в котором нужно вызвать данный метод.

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Main main = new Main();
            System.out.println("Division result = " + main.playfulArray());
        } catch (OutOfBoundsException | NotNumberException | ZeroException e) {
            System.err.println(e.getMessage());
        }
    }

    public double playfulArray() throws OutOfBoundsException, NotNumberException, ZeroException {
        System.out.print("\nPlease enter the number from zero to 29: ");
        if (!console.hasNextInt()) throw new NotNumberException();
        int consoleI = console.nextInt();
        console.nextLine();
        int randomI = (int) ((Math.random() * 30) + 1);
        if (consoleI < 0 || consoleI >= randomI) throw new OutOfBoundsException();
        int[] myArray = new int[randomI];
        for (int i = 0; i < randomI; i++) {
            myArray[i] = (int) (Math.random() * 31);
        }
        if (myArray[consoleI] == 0 || myArray[0] == 0) throw new ZeroException();
        System.out.println("Array size = " + randomI);
        System.out.println(Arrays.toString(myArray));
        return (double) myArray[consoleI] / myArray[0];
    }
}