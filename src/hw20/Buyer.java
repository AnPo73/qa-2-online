package hw20;

public class Buyer extends Human {
    private int age;
    private boolean isDiscountCard;

    public Buyer(String humanName, int age) {
        super(humanName);
        this.age = age;
    }

    public Buyer(String humanName, int age, boolean isDiscountCard) {
        super(humanName);
        this.age = age;
        this.isDiscountCard = isDiscountCard;
    }

    public int getAge() {
        return age;
    }

    public boolean isDiscountCard() {
        return isDiscountCard;
    }

    @Override
    void role() {
        if (isDiscountCard) {
            System.out.println("Я покупатель " + getHumanName() + ", мне " + age + ", у меня есть дисконтная карта.");
        } else {
            System.out.println("Я покупатель " + getHumanName() + ", мне " + age + ", у меня нет дисконтной карты.");
        }
    }
}