package hw16;

//        (Использовать операторы if-else-if)
//        Пользовать ель вводит с клавиатуры числа:
//        Если число равно 1, то выводим на консоль "Понедельник";
//        Если число равно 2, то выводим на консоль "Вторник";
//        Если число равно 3, то выводим на консоль "Среда";
//        Если число равно 4, то выводим на консоль "Четверг";
//        Если число равно 5, то выводим на консоль "Пятница";
//        Если число равно 6, то выводим на консоль "Суббота";
//        Если число равно 7, то выводим на консоль "Воскресенье";
//        В противном случае выводим текст:
//        "Лучше бы сегодня была пятница".

import java.util.Scanner;

public class Task3 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Enter a number from 1 to 7: ");
        String s = console.nextLine();
        if (s.equals("1")) {
            System.out.println("Понедельник");
        } else if (s.equals("2")) {
            System.out.println("Вторник");
        } else if (s.equals("3")) {
            System.out.println("Среда");
        } else if (s.equals("4")) {
            System.out.println("Четверг");
        } else if (s.equals("5")) {
            System.out.println("Пятница");
        } else if (s.equals("6")) {
            System.out.println("Суббота");
        } else if (s.equals("7")) {
            System.out.println("Воскресенье");
        } else {
            System.out.println("Лучше бы сегодня была пятница");
        }
    }
}