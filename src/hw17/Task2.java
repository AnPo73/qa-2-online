package hw17;

//        Есть одномерный массив из 10 элементов, заполненный
//        случайными числами. Пользователь вводит с клавиатуры
//        число. Программа показывает есть ли такое число в созданном
//        до этого массиве.

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[10];

        System.out.print("\nEnter upper range limit: ");
        notNumber();
        int range = console.nextInt();
        console.nextLine();
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * range);
        }
        System.out.println("Received array: " + Arrays.toString(array));

        System.out.print("\nEnter any number between 0 and " + (range - 1) + ": ");
        notNumber();
        int number = console.nextInt();
        console.nextLine();
        boolean isTrue = false;
        for (int j : array) {
            if (number == j) {
                isTrue = true;
                break;
            }
        }
        System.out.print("is there such a number in the created array? ");
        System.err.println(String.valueOf(isTrue).toUpperCase());
    }

    public static void notNumber() {
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
    }
}