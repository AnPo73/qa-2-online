package hw20;

public class Seller extends Human {
    private boolean isHonest;

    public Seller(String humanName) {
        super(humanName);
    }

    public Seller(String humanName, boolean isHonest) {
        super(humanName);
        this.isHonest = isHonest;
    }

    public boolean isHonest() {
        return isHonest;
    }

    @Override
    void role() {
        if (isHonest) {
            System.out.println("Я продавец " + getHumanName() + ", я честный.");
        } else {
            System.out.println("Я продавец " + getHumanName() + ", я нечестный.");
        }
    }
}