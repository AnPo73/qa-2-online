package testing.hw28;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FoxtrotSearchParametersTest extends BaseTest {
    private static class Url {
        private static final String foxtrot = "https://www.foxtrot.com.ua/";
    }

    private static class Locators {
        private static final By iAmLookingFor = By.cssSelector("[placeholder = 'Я шукаю ...']");
        private static final By searchPageBox = By.cssSelector(".search-page__box-title>label");
    }

    private static class Labels {
        private final static String foundByRequest = "Знайдено по запиту ";
        private final static String searchResults = "Результати пошуку";
    }

    @Test
    @Parameters({"input"})
    public void foxtrotSearchParameters(String input) {
        driver.get(Url.foxtrot);
        waiters.waitForVisibilityOf(Locators.iAmLookingFor);
        driver.findElement(Locators.iAmLookingFor).sendKeys(input + "\n");
        waiters.waitForTitleContains(Labels.foundByRequest);

        if (driver.findElements(By.tagName("h1")).get(0).getText().contains(Labels.foundByRequest)) {
            String actual = driver.findElements(By.tagName("h1")).get(0).getText().replace(Labels.foundByRequest, "");
            String expected = "«" + input + "»";
            assertTrue(actual.equalsIgnoreCase(expected));
        }

        if (driver.findElements(By.tagName("h1")).get(0).getText().contains(Labels.searchResults)) {
            String actual = driver.findElement(Locators.searchPageBox).getText();
            String expected = "«" + input + "»";
            assertTrue(actual.equalsIgnoreCase(expected));
        }
    }
}