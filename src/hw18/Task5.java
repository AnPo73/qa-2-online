package hw18;

//        Создать класс Конспект. Класс должен содержать следующие поля:
//        название предмета;
//        ФИО студента;
//        количество страниц;
//        год выпуска;
//        цвет обложки.
//        Название заведения, где учится студент;
//
//        Создайте публичные методы для получения доступа к полям класса,
//        а также методы для присваивания значений полям класса.

public class Task5 {
    public static void main(String[] args) {
        Outline student1 = new Outline();
        student1.setSubject("Programming");
        student1.setFullName("Jack Smith");
        student1.setPageCount(50);
        student1.setGraduationYear(2023);
        student1.setCoverColor("Green");
        student1.setNameOfInstitution("Kyiv Polytechnic Institute");
        System.out.println("\n" + student1.getSubject() + "\n" + student1.getFullName() + "\n" + student1.getPageCount() + "\n"
                + student1.getGraduationYear() + "\n" + student1.getCoverColor() + "\n" + student1.getNameOfInstitution());
    }
}

class Outline {
    private String subject;
    private String fullName;
    private int pageCount;
    private int graduationYear;
    private String coverColor;
    private String nameOfInstitution;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public String getCoverColor() {
        return coverColor;
    }

    public void setCoverColor(String coverColor) {
        this.coverColor = coverColor;
    }

    public String getNameOfInstitution() {
        return nameOfInstitution;
    }

    public void setNameOfInstitution(String nameOfInstitution) {
        this.nameOfInstitution = nameOfInstitution;
    }
}