package testing.hw27;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Ex1Test extends BaseTest {
    private static class Url {
        private static final String danIt = "https://dan-it.com.ua/uk/";
    }

    private static class Locators {
        private static final By java = By.xpath("//ul[@class = 'programms__list']/li[14]");
        private static final By qa = By.xpath("//ul[@class = 'programms__list']/li[15]");
        private static final By frontEnd = By.xpath("//ul[@class = 'programms__list']/li[1]");
    }

    @Test
    public void numberOfOpenWindows() {
        Ex1Test ex1Test = new Ex1Test();
        Actions actions = new Actions(driver);
        driver.get(Url.danIt);
        waiters.waitForElementToBeClickable(Locators.java);
        ex1Test.openElementInNewWindow(driver, actions, Locators.java);
        waiters.waitForElementToBeClickable(Locators.qa);
        ex1Test.openElementInNewWindow(driver, actions, Locators.qa);
        waiters.waitForElementToBeClickable(Locators.frontEnd);
        ex1Test.openElementInNewWindow(driver, actions, Locators.frontEnd);
        assertEquals(driver.getWindowHandles().size(), 4, "The number of windows does not match");
    }

    public void openElementInNewWindow(WebDriver driver, Actions actions, By locator) {
        WebElement element = driver.findElement(locator);
        actions.moveToElement(element).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build().perform();
    }
}