package hw20;

public abstract class Human {
    private String humanName;

    public Human(String humanName) {
        this.humanName = humanName;
    }

    public String getHumanName() {
        return humanName;
    }

    abstract void role();
}