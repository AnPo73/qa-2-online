package testing.hw27;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Ex5Test extends BaseTest {
    private static class Url {
        private static final String onlyTestingBlog = "http://only-testing-blog.blogspot.com/2014/01/textbox.html";
    }

    private static class Locators {
        private static final By showMeAlert = By.cssSelector("[value = 'Show Me Alert']");
    }

    @Test
    public void checkboxChecking() {
        driver.get(Url.onlyTestingBlog);
        waiters.waitForPresenceOfElementLocated(Locators.showMeAlert);
        WebElement showMeAlert = driver.findElement(Locators.showMeAlert);
        showMeAlert.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Alert alert = driver.switchTo().alert();
        assertEquals(alert.getText(), "Hi.. This is alert message!",
                "Modal window does not contain required text");
        alert.accept();
    }
}