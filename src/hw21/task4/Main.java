package hw21.task4;

//        Реализовать данную ситуацию в коде.
//        Отцу у которого есть имя и определенная сумма в кармане должен сходить в магазин.
//        В магазине есть 5 типов продуктов: яблоко, картошка, молоко, пиво и табак.
//        У каждого товара есть своя цена.
//        Отцу необходимо сделать покупку, максимум он может купить только два типа продуктов.
//        Но любой тип продуктов может быть куплен в любом количестве.
//        Т.е. он может купить только молоко. Или купить 3 молока, и 2 яблока. Но он не может
//        купить яблоко, три пива и две картофелины.
//        Неприятная ситуация произойдет в нескольких ситуациях:
//        1) Если сумма покупки превышает количество денег в кармане отца.
//        2) Если он принесет домой пиво либо табак.
//        3) Если количество хотя бы одного из купленных товаров не будет делиться на три.
//        (т.е. он не сможет поровну разделить купленный продукт между собой, женой и ребенком)
//        Реализовать данную ситуацию при помощи объектно ориентированного подхода.
//        Постараться использовать перечислительный тип данных Enum. А также исключения.
//        Постараться обработать все исключения в стороннем методе, а не в теле метода созданного у отца.
//        В случае если отцу удастся избежать неприятной ситуации на консоль должно вывестись сообщение
//        примерно такого типа:
//        Уважаемый Валентин стоимость ваших покупок 150.
//        Вы приобрели APPLE в количестве 3, и MILK в количестве 6.

public class Main {
    public static void main(String[] args) {
        Shop shop = new Shop();
        try {
//            shop.purchase(new Father("Jack", 140), new Product(Food.BEER, 40, 4));
//            shop.purchase(new Father("Jack", 160), new Product(Food.BEER, 40, 4));
//            shop.purchase(new Father("Jack", 160), new Product(Food.APPLE, 30, 4));
            shop.purchase(new Father("Jack", 180), new Product(Food.APPLE, 30, 6));
//            shop.purchase(new Father("Peter", 200), new Product(Food.TOBACCO, 70, 2), new Product(Food.MILK, 35, 2));
//            shop.purchase(new Father("Peter", 210), new Product(Food.TOBACCO, 70, 2), new Product(Food.MILK, 35, 2));
//            shop.purchase(new Father("Peter", 220), new Product(Food.POTATO, 40, 2), new Product(Food.TOBACCO, 70, 2));
//            shop.purchase(new Father("Peter", 210), new Product(Food.POTATO, 40, 2), new Product(Food.MILK, 35, 3));
//            shop.purchase(new Father("Peter", 210), new Product(Food.POTATO, 40, 3), new Product(Food.MILK, 35, 2));
            shop.purchase(new Father("Peter", 225), new Product(Food.POTATO, 40, 3), new Product(Food.MILK, 35, 3));
        } catch (NotEnoughMoneyException | BeerOrTobaccoException | NotMultipleOfThreeException e) {
            System.err.println(e.getMessage());
        }
    }
}