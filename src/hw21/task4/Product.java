package hw21.task4;

public class Product {
    private Food food;
    private int price;
    private int qty;

    public Product(Food food, int price, int qty) {
        this.food = food;
        this.price = price;
        this.qty = qty;
    }

    public Food getFood() {
        return food;
    }

    public int getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }
}