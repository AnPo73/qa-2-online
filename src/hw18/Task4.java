package hw18;

//        Написать перегруженный метод, который может:
//        - не принимать никаких значений, тогда он будет выводить на консоль сообщение типа:
//        "Я пустой".
//        - Принимать в качестве параметров String, тогда он будет выводить на консоль это сообщение.
//        - Принимать в качестве параметров массив строк, тогда он будет выводить на консоль все его значения
//        через пробел.
//        - Принимать в качестве параметра массив чисел, тогда он будет выводить на консоль сумму элементов
//        массива.
//        - Принимать в качестве параметров число и строку, тогда он будет выводить на консоль сообщение типа:
//        "Ваше сообщение - "%%%%%%%%", ваше число -  $",
//        где "%%%%%%%%" и $ ваши введенные строка и число соответственно.

public class Task4 {
    public static void main(String[] args) {
        overloaded();
        overloaded("Принимать в качестве параметров String, тогда он будет выводить на консоль это сообщение");
        String[] strings = new String[]{"Принимать в качестве", "параметров массив строк,", "тогда он будет",
                "выводить на консоль", "все его значения", "через пробел" };
        overloaded(strings);
        int[] ints = new int[]{1, 2, 3, 4, 5};
        overloaded(ints);
        overloaded("Україна Понад Усе!",100500);
    }

    static void overloaded() {
        System.out.println("\nЯ пустой");
    }

    static void overloaded(String s) {
        System.out.println(s);
    }

    static void overloaded(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            if (i < strings.length - 1) {
                System.out.print(strings[i] + " ");
            } else {
                System.out.println(strings[i]);
            }
        }
    }

    static void overloaded(int[] ints) {
        int sum = 0;
        for (int i : ints) {
            sum += i;
        }
        System.out.println(sum);
    }

    static void overloaded(String s, int i) {
        System.out.printf("Ваше сообщение - \"%1$s\", ваше число - %2$d\n", s, i);
    }
}