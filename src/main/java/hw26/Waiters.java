package hw26;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.function.Function;

public class Waiters {
    private final WebDriver driver;
    private static final long EXPLICIT_WAIT = 20L;

    public Waiters(WebDriver driver) {
        this.driver = driver;
    }

// - presenceOfElementLocated()

    public void waitForPresenceOfElementLocated(By locator) {
        waitForFunction(ExpectedConditions
                .presenceOfElementLocated(locator), EXPLICIT_WAIT);
    }

    public WebElement waitForPresenceOfElementLocatedReturn(By locator) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

// - textToBePresentInElementValue()

    public void waitForTextToBePresentInElementValue(WebElement element, String text) {
        waitForFunction(ExpectedConditions
                .textToBePresentInElementValue(element, text), EXPLICIT_WAIT);
    }

    public void waitForTextToBePresentInElementValue(By locator, String text) {
        waitForFunction(ExpectedConditions
                .textToBePresentInElementValue(driver.findElement(locator), text), EXPLICIT_WAIT);
    }

// - invisibilityOf()

    public void waitForInvisibilityOf(WebElement element) {
        waitForFunction(ExpectedConditions
                .invisibilityOf(element), EXPLICIT_WAIT);
    }

    public void waitForInvisibilityOf(By locator) {
        waitForFunction(ExpectedConditions
                .invisibilityOf(driver.findElement(locator)), EXPLICIT_WAIT);
    }

// - titleContains()

    public void waitForTitleContains(String title) {
        waitForFunction(ExpectedConditions
                .titleContains(title), EXPLICIT_WAIT);
    }

// - titleIs()

    public void waitForTitleIs(String title) {
        waitForFunction(ExpectedConditions
                .titleIs(title), EXPLICIT_WAIT);
    }

// - elementToBeClickable()

    public void waitForElementToBeClickable(WebElement element) {
        waitForFunction(ExpectedConditions
                .elementToBeClickable(element), EXPLICIT_WAIT);
    }

    public void waitForElementToBeClickable(By locator) {
        waitForFunction(ExpectedConditions
                .elementToBeClickable(driver.findElement(locator)), EXPLICIT_WAIT);
    }

    public WebElement waitForElementToBeClickableReturn(WebElement element) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public WebElement waitForElementToBeClickableReturn(By locator) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
    }

// - elementToBeSelected()

    public void waitForElementToBeSelected(WebElement element) {
        waitForFunction(ExpectedConditions
                .elementToBeSelected(element), EXPLICIT_WAIT);
    }

    public void waitForElementToBeSelected(By locator) {
        waitForFunction(ExpectedConditions
                .elementToBeSelected(driver.findElement(locator)), EXPLICIT_WAIT);
    }

// - elementSelectionStateToBe()

    public void waitForElementSelectionStateToBe(WebElement element, boolean selected) {
        waitForFunction(ExpectedConditions
                .elementSelectionStateToBe(element, selected), EXPLICIT_WAIT);
    }

    public void waitForElementSelectionStateToBe(By locator, boolean selected) {
        waitForFunction(ExpectedConditions
                .elementSelectionStateToBe(driver.findElement(locator), selected), EXPLICIT_WAIT);
    }

// - visibilityOfElementLocated()

    public void waitForVisibilityOfElementLocated(By locator) {
        waitForFunction(ExpectedConditions
                .visibilityOfElementLocated(locator), EXPLICIT_WAIT);
    }

    public WebElement waitForVisibilityOfElementLocatedReturn(By locator) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

// - frameToBeAvailableAndSwitchToIt()

    public void waitForFrameToBeAvailableAndSwitchToIt(WebElement element) {
        waitForFunction(ExpectedConditions
                .frameToBeAvailableAndSwitchToIt(element), EXPLICIT_WAIT);
    }

    public void waitForFrameToBeAvailableAndSwitchToIt(By locator) {
        waitForFunction(ExpectedConditions
                .frameToBeAvailableAndSwitchToIt(driver.findElement(locator)), EXPLICIT_WAIT);
    }

// - alertIsPresent()

    public void waitForAlertIsPresent() {
        waitForFunction(ExpectedConditions
                .alertIsPresent(), EXPLICIT_WAIT);
    }

// - visibilityOf()

    public void waitForVisibilityOf(WebElement element) {
        waitForFunction(ExpectedConditions
                .visibilityOf(element), EXPLICIT_WAIT);
    }

    public void waitForVisibilityOf(By locator) {
        waitForFunction(ExpectedConditions
                .visibilityOf(driver.findElement(locator)), EXPLICIT_WAIT);
    }

    public WebElement waitForVisibilityOfReturn(WebElement element) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement waitForVisibilityOfReturn(By locator) {
        return fluentWait(EXPLICIT_WAIT)
                .until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
    }

    private FluentWait<WebDriver> fluentWait(Long duration) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(duration))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(InvalidElementStateException.class)
                .ignoring(StaleElementReferenceException.class);
    }

    private void waitForFunction(Function function, Long timeOutInSeconds) {
        FluentWait<WebDriver> wait = fluentWait(timeOutInSeconds);
        wait.until(function);
    }

    public void waitSomeSeconds(int seconds) {
        int milliseconds = seconds * 1000;
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}