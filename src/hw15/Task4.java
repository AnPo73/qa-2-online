package hw15;

//        Вывести следующие сообщения в отформатированном виде (необходимо добавить спецификаторы формата):
//
//        а) Строка: Чтобы написать код нам нужны 1)Язык программирования 2)Среда разработки 3)Воодушевление
//        Вывод: Чтобы написать код нам нужны
//        1)Язык программирования
//        2)Среда разработки
//        3)Воодушевление
//
//        б) Строка: Число {X} больше {Y}, и это {True/False}
//        Для аргументов создаем отдельно переменные.
//        Вместо {X},{Y},{True/False} вставляем необходимые спецификаторы формата.
//        Вывод: Число 5 больше 7, и это false.

import java.util.Scanner;

public class Task4 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        String s = "Чтобы написать код нам нужны 1)Язык программирования 2)Среда разработки 3)Воодушевление";
        s = s.replace(" 1)", "%n1)");
        s = s.replace(" 2)", "%n2)");
        s = s.replace(" 3)", "%n3)");
        System.out.printf(s + "%n" + "%n");

        int[] ints = new int[2];
        for (int i = 0; i < 2; i++) {
            System.out.print("Enter any integer: ");
            while (!console.hasNextInt()) {
                System.out.print("Invalid value, please try again: ");
                console.nextLine();
            }
            ints[i] = console.nextInt();
            console.nextLine();
        }
        int x = ints[0];
        int y = ints[1];
        boolean isNew = x > y;
        System.out.printf("Число %1$s больше %2$s, и это %3$b", x, y, isNew);
    }
}