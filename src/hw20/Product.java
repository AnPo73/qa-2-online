package hw20;

public class Product {
    private String productName;
    private int cost;
    private boolean isAlcohol;

    public Product(String productName, int cost) {
        this.productName = productName;
        this.cost = cost;
    }

    public Product(String productName, int cost, boolean isAlcohol) {
        this.productName = productName;
        this.cost = cost;
        this.isAlcohol = isAlcohol;
    }

    public String getProductName() {
        return productName;
    }

    public int getCost() {
        return cost;
    }

    public boolean isAlcohol() {
        return isAlcohol;
    }
}