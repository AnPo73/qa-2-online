package hw21.task4;

import static hw21.task4.Food.BEER;
import static hw21.task4.Food.TOBACCO;

public class Shop {
    public void purchase(Father father, Product product1) throws NotEnoughMoneyException, BeerOrTobaccoException, NotMultipleOfThreeException {
        int purchaseCost = product1.getPrice() * product1.getQty();
        if (father.getMoney() < purchaseCost) throw new NotEnoughMoneyException();
        if (product1.getFood() == BEER || product1.getFood() == TOBACCO) throw new BeerOrTobaccoException();
        if (product1.getQty() % 3 != 0) throw new NotMultipleOfThreeException();
        System.out.printf("%nDear %1$s, the cost of your purchases is %2$d.%nYou purchased %3$s in qty %4$d.%n",
                father.getName(), purchaseCost, product1.getFood(), product1.getQty());
    }

    public void purchase(Father father, Product product1, Product product2) throws NotEnoughMoneyException,
            BeerOrTobaccoException, NotMultipleOfThreeException {
        int purchaseCost = product1.getPrice() * product1.getQty() + product2.getPrice() * product2.getQty();
        if (father.getMoney() < purchaseCost) throw new NotEnoughMoneyException();
        if (product1.getFood() == BEER || product1.getFood() == TOBACCO ||
                product2.getFood() == BEER || product2.getFood() == TOBACCO) throw new BeerOrTobaccoException();
        if (product1.getQty() % 3 != 0 || product2.getQty() % 3 != 0) throw new NotMultipleOfThreeException();
        System.out.printf("%nDear %1$s, the cost of your purchases is %2$d.%nYou purchased %3$s in qty %4$d and %5$s in qty %6$d.%n",
                father.getName(), purchaseCost, product1.getFood(), product1.getQty(), product2.getFood(), product2.getQty());
    }
}