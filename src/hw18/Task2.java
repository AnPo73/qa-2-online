package hw18;

//        Написать метод, принимающий в качестве параметра массив целых чисел. И выводит на экран все четные числа списком,
//        а также нечетные числа списком.

public class Task2 {
    public static void main(String[] args) {
        int[] myArray = new int[10];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) (Math.random() * 101);
        }
        evenOdd(myArray);
    }

    static void evenOdd(int[] myArray) {
        for (int i : myArray)
            if (i % 2 == 0)
                System.out.print(i + " ");
        System.out.println();
        for (int i : myArray)
            if (i % 2 != 0)
                System.out.print(i + " ");
    }
}