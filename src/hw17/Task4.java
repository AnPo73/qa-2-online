package hw17;

//        Имеется массив, размер которого должен быть четным числом и этот размер вводится пользователем с клавиатуры.
//        Значения элементов задаются случайным образом в диапазоне от 0 до 100.
//        Отсортировать первую половину данного массива по возрастанию, а вторую по
//        убыванию. Вывести на экран изначальный и отсортированный массив.

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Task4 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("\nEnter array size (even number): ");
        notNumber();
        int arraySize = console.nextInt();
        console.nextLine();
        while (!(arraySize % 2 == 0)) {
            System.out.print("Invalid value, please try again: ");
            notNumber();
            arraySize = console.nextInt();
            console.nextLine();
        }

        int[] array = new int[arraySize];
        int[] sortedArray = new int[arraySize];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 101);
        }
        int[] temp1 = Arrays.copyOfRange(array, 0, array.length / 2);
        int[] temp2 = Arrays.copyOfRange(array, array.length / 2, array.length);
        Arrays.sort(temp1);
        Integer[] temp3 = new Integer[temp2.length];
        for (int i = 0; i < temp2.length; i++) {
            temp3[i] = temp2[i];
        }
        Arrays.sort(temp3, Collections.reverseOrder());
        for (int i = 0; i < temp3.length; i++) {
            temp2[i] = temp3[i];
        }
        System.arraycopy(temp1, 0, sortedArray, 0, array.length / 2);
        System.arraycopy(temp2, 0, sortedArray, array.length / 2, array.length / 2);
        System.out.println("array = " + Arrays.toString(array));
        System.out.println("sortedArray = " + Arrays.toString(sortedArray));
    }

    public static void notNumber() {
        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }
    }
}