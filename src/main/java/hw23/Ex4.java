package hw23;

//        Написать метод, который будет выводить в консоль тексты всех
//        элементов, которые можно найти по заданному параметру.
//        Например, при помощи него можно будет получить значения всех элементов
//        из списков заданных на "Рисунок 4.png" и "Рисунок 5.png".

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Ex4 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/ua/";
    }

    private static class Locators {
        private static final By menuTitleLink = By.cssSelector(".products-menu__title-link");
        private static final By footerMenuItem = By.cssSelector(".footer__menu-item");
    }

    public static void main(String[] args) {
        Ex4 ex4 = new Ex4();
        ex4.AllElementsTexts(Url.uhomki, Locators.menuTitleLink);
        ex4.AllElementsTexts(Url.uhomki, Locators.footerMenuItem);
    }

    public void AllElementsTexts(String url, By locator) {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(url);
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            String text = element.getText();
            System.out.println(text);
        }
        driver.quit();
    }
}