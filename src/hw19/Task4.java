package hw19;

//        Создать класс калькулятор.
//        В нем создать методы:
//        sum, minus, multiply, division.
//        Сложение, вычитание, умножение и деление соответственно.
//        Каждый метод принимает в качестве параметров два значения типа double.
//        И выводит в консоль результат действия.
//        Также в классе есть метод старт. Который выводит сообщение в консоль, что
//        калькулятор запущен. И предлагает ввести действие в вашей консоли.
//        Калькулятор должен принимать только следующие типы действий:
//        2+4;    - пример синтаксиса сложения;
//        5-6;    - пример синтаксиса вычитания;
//        25*3;   - пример синтаксиса умножения;
//        34/3;   - пример синтаксиса деления;
//        После ввода действия на консоль выводится ответ этого действия.
//        И после снова выводится сообщение о предложении ввести действие.
//        в случае ввода другого синтаксиса на консоль возвращается сообщение:
//        "Введите корректное действие".
//        И после снова выводится сообщение о предложении ввести действие.
//        Программа закрывается после ввода команды Stop.
//        Перед закрытием на консоль должно выводится сообщение:
//        "Калькулятор закрыт".

import java.util.Scanner;

import static hw19.Task4.console;

public class Task4 {
    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.start();
    }
}

class Calculator {
    public void sum(double d1, double d2) {
        System.out.println("Answer: " + (d1 + d2));
    }

    public void minus(double d1, double d2) {
        System.out.println("Answer: " + (d1 - d2));
    }

    public void multiply(double d1, double d2) {
        System.out.println("Answer: " + (d1 * d2));
    }

    public void division(double d1, double d2) {
        System.out.println("Answer: " + (d1 / d2));
    }

    public void start() {
        System.out.println("Calculator started...");
        System.out.print("Enter expression to calculate or \"STOP\" to exit: ");
        String s = console.nextLine().trim().toLowerCase();

        if (s.indexOf('+') != -1) {
            String[] numbers = s.split("\\+");
            double d1 = Double.parseDouble(numbers[0]), d2 = Double.parseDouble(numbers[1]);
            sum(d1, d2);
            start();
        } else if (s.indexOf('-') != -1) {
            String[] numbers = s.split("-");
            double d1 = Double.parseDouble(numbers[0]), d2 = Double.parseDouble(numbers[1]);
            minus(d1, d2);
            start();
        } else if (s.indexOf('*') != -1) {
            String[] numbers = s.split("\\*");
            double d1 = Double.parseDouble(numbers[0]), d2 = Double.parseDouble(numbers[1]);
            multiply(d1, d2);
            start();
        } else if (s.indexOf('/') != -1) {
            String[] numbers = s.split("/");
            double d1 = Double.parseDouble(numbers[0]), d2 = Double.parseDouble(numbers[1]);
            division(d1, d2);
            start();
        } else if (s.equals("stop")) {
            System.out.println("Calculator closed...");
        } else {
            System.out.println("Please enter a valid expression...");
            start();
        }
    }
}