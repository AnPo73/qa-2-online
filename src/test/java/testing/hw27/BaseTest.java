package testing.hw27;

import hw26.Waiters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    static WebDriver driver;
    static Waiters waiters;

    public void initialDriver() {
        driver = new ChromeDriver();
    }

    public void waiters() {
        waiters = new Waiters(driver);
    }

    public void setDriver() {
        driver.manage().window().maximize();
    }

    public void closeDriver() {
        driver.quit();
    }

    @BeforeClass
    public void startTest() {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        initialDriver();
        waiters();
        setDriver();
    }

    @AfterClass
    public void stopTest() {
        closeDriver();
    }
}