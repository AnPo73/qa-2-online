package hw20;

public class Shop implements Cost {
    private String shopName;

    public Shop(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public void returnCost(Buyer buyer, Seller seller, Product product) {
        if (buyer.getAge() < 18 && seller.isHonest() && product.isAlcohol()) {
            System.out.printf("Уважаемый %1$s, продавец нашего магазина \"%3$s\" %2$s не продаст Вам товар \"%4$s\"." +
                            " Вам %5$d, мы не продаем алкоголь лицам, не достигшим 18 лет.\n",
                    buyer.getHumanName(), seller.getHumanName(), shopName, product.getProductName(), buyer.getAge());
        } else if (buyer.isDiscountCard()) {
            System.out.printf("Уважаемый %1$s, продавец нашего магазина \"%3$s\" %2$s продаст Вам товар \"%4$s\"." +
                            " Вам %5$d, у Вас есть дисконтная карта, к оплате %6$d грн.\n",
                    buyer.getHumanName(), seller.getHumanName(), shopName, product.getProductName(), buyer.getAge(), product.getCost() / 10 * 9);
        } else {
            System.out.printf("Уважаемый %1$s, продавец нашего магазина \"%3$s\" %2$s продаст Вам товар \"%4$s\"." +
                            " Вам %5$d, у Вас нет дисконтной карты, к оплате %6$d грн.\n",
                    buyer.getHumanName(), seller.getHumanName(), shopName, product.getProductName(), buyer.getAge(), product.getCost());
        }
    }
}