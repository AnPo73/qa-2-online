package hw18;

//        Написать метод, принимающий в качестве параметров два числа,
//        который будет выводить на консоль сумму этих двух чисел.
//
//        Написать второй метод, который будет возвращать сумму двух чисел,
//        которые он будет принимать в параметре метода.
//
//        Вывести на консоль сумму двух любых чисел при помощи первого и второго метода.

public class Task1 {
    public static void main(String[] args) {
        int range = 51;
        int i1 = (int) (Math.random() * range), i2 = (int) (Math.random() * range);
        System.out.println("\ni1 = " + i1 + "\ni2 = " + i2);
        sum1(i1, i2);
        System.out.println("sum2 = " + sum2(i1, i2));
    }

    static void sum1(int i1, int i2) {
        System.out.println("sum1 = " + (i1 + i2));
    }

    static int sum2(int i1, int i2) {
        return i1 + i2;
    }
}