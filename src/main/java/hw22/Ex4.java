package hw22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Ex4 {
    private static class Url {
        private static final String uhomki = "https://uhomki.com.ua/koshki/1074/";
    }

    private static class Locators {
        private static final By locator1 = By.xpath("//a[@href = '/ru/akvariumistika/1124/']");
        private static final By locator2 = By.xpath("//a[@href = '/ru/akvariumy/1130/']");
    }

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get(Url.uhomki);
        WebElement hoverable = driver.findElement(Locators.locator1);
        new Actions(driver).moveToElement(hoverable).perform();
        Thread.sleep(1000);
        driver.findElement(Locators.locator2).click();
    }
}