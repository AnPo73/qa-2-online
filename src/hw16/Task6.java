package hw16;

//        Написать программу, условно для склада приема металла. Представим, что склад
//        может хранить определенный вес металла. Пользователь вводит с
//        клавиатуры вес, который может храниться на складе. Дальше пользователь вводит
//        с клавиатуры вес, который условно собирается сдать на склад пользователь.
//        Программа должна после каждой сдачи металла показывать сколько веса еще может принять
//        склад. Если пользователь хочет сдать металла больше чем осталось места на складе,
//        то программа не дает ему это сделать и уведомляет пользователя, о невозможности данной операции.
//        Если пользователь сдает металл весом меньше чем 5, программа тоже предупреждает
//        о невозможности приемки такого малого веса. Программа завершается, когда
//        место на складе закончилось.

import java.util.Scanner;

public class Task6 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Enter warehouse capacity (kg): ");

        while (!console.hasNextInt()) {
            System.out.print("Invalid value, please try again: ");
            console.nextLine();
        }

        int wRem = console.nextInt();
        console.nextLine();
        int w = 0;

        while (wRem > 4) {
            System.out.print("Enter the handed over weight (kg): ");

            if (!console.hasNextInt()) {
                System.out.println("Invalid value, please try again... ");
                console.nextLine();
                continue;
            }

            w = console.nextInt();
            console.nextLine();

            if (w < 5) {
                System.out.println("Weight too low, operation rejected...");
                continue;
            } else if (w > wRem) {
                System.out.println("There is not enough space in the warehouse...");
                continue;
            } else {
                wRem = wRem - w;
            }

            if (wRem < 5) {
                System.out.println("\nThe warehouse is completely full...");
                break;
            } else {
                System.out.println("\nRemaining space in the warehouse (kg): " + (wRem));
            }
        }
    }
}