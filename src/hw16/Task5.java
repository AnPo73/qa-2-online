package hw16;

//        Написать программу в которой пользователь вводит с клавиатуры число,
//        а программа определяет, является оно палиндромом или нет.
//        И выводит данную информацию на экран.

import java.util.Scanner;

public class Task5 {

//    зараз
//    комок
//    шалаш
//    civic
//    madam
//    radar

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Please type in a number or word: ");
        String s = console.nextLine().trim().toLowerCase();
        int i = 0;
        int length = s.length();
        boolean isPalindrome = false;
        while (i < (length / 2)) {
            isPalindrome = false;
            if (s.charAt(i) == s.charAt(length - i - 1)) {
                isPalindrome = true;
                i++;
            } else {
                break;
            }
        }
        String isPalindromeString = String.valueOf(isPalindrome);
        System.out.println("Is it palindrome? " + isPalindromeString.toUpperCase());
    }
}