package hw21.task2;

//        Написать метод в котором будет создан бесконечный цикл по заполнению
//        саморасширяющегося массива с типом Integer. Первое значение элемента массива должно быть 2147483638,
//        и с каждой итерацией цикла значение элемента массива должно увеличиваться на 1.
//        Т.е. значение второго элемента массива 2147483639, третьего 2147483640 и т.д.
//        Исключительная ситуация должна возникать когда значение элемента массива будет превышать максимально
//        допустимое значение типа int.
//        Генерация этого типа исключения в теле бесконечного цикла должно помочь выйти из данного цикла.
//        Обработку исключительной ситуации необходимо также осуществить в теле данного метода.
//        Данный метод должен в любом случае выводить в консоль данный саморасширяющийся массив.

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.endlessLoop();
    }

    public void endlessLoop() {
        ArrayList<String> arrayList = new ArrayList<>();
        int arrayValue = 2147483637;
        try {
            for (int i = 0; ; i++) {
                arrayValue += 1;
                if (2147483647 - arrayValue < 0) throw new MaxValueException();
                arrayList.add(i, String.valueOf(arrayValue));
            }
        } catch (MaxValueException e) {
            System.err.println(e.getMessage());
        }
        arrayList.forEach(System.out::println);
    }
}