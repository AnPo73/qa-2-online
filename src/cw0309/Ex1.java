package cw0309;

//        Пользователь вводит с клавиатуры предложение.
//        Если количество слов в нем больше трех, но меньше девяти и это количество четное,
//        то в консоль выводится сообщение "выполнилось первое условие".
//        Если количество слов больше двенадцати, но меньше двадцати, и это количество не
//        делится на три, то в консоль выводится сообщение "выполнилось второе условие".
//        Если количество слов в предложении равно трем, то в консоль выводится сообщение
//        "выполнилось третье условие".
//        В противном случае вывести сообщение "Не выполнилось ни одно условие".

import java.util.Scanner;

public class Ex1 {

    static Scanner console = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Please enter the text: ");
        String line = console.nextLine();
        String[] lineSplit = line.split(" ");
        int lineLength = lineSplit.length;
        System.out.println(lineLength);
        if (lineLength > 3 && lineLength < 9 && lineLength % 2 == 0) {
            System.out.println("выполнилось первое условие");
        }
        if (lineLength > 12 && lineLength < 20 && !(lineLength % 3 == 0)) {
            System.out.println("выполнилось второе условие");
        }
        if (lineLength == 3) {
            System.out.println("выполнилось третье условие");
        }
    }
}
