package hw21.task4;

public class NotEnoughMoneyException extends Exception{
    public String getMessage() {
        return "\nNot enough money...";
    }
}
