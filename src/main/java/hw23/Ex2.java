package hw23;

//        Написать метод в параметры которого принимаются два ВебЭлемента.
//        Метод выводит в консоль информацию какой из двух элементов располагается выше на странице,
//        какой из элементов располагается левее на странице,
//        а также какой из элементов занимает большую площадь.
//        Параметры метода могут также включать в себя другие аргументы, если это необходимо.

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex2 {
    private static class Url {
        private static final String topAutogas = "https://top-autogas.com.ua/";
    }

    private static class Locators {
        private static final By ClampsAndTapeXpath = By.xpath("//ul[@id = 'menu-list']/li[21]");
        private static final By ClampsAndTapeCssSelector = By.cssSelector("[href = 'izolenta']");
        private static final By HboCylindersXpath = By.xpath("//div[@class = 'row imgcategory']/div[3]");
        private static final By HboCylindersCssSelector = By.cssSelector("[href = 'https://top-autogas.com.ua/ballony/']");
    }

    public static void main(String[] args) {
        Ex2 ex2 = new Ex2();
        ex2.locationSizeComparison(Url.topAutogas, Locators.ClampsAndTapeCssSelector, Locators.HboCylindersCssSelector);
        ex2.locationSizeComparison(Url.topAutogas, Locators.HboCylindersXpath, Locators.ClampsAndTapeXpath);
    }

    public void locationSizeComparison(String url, By locator1, By locator2) {
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(url);

        WebElement element1 = driver.findElement(locator1);
        WebElement element2 = driver.findElement(locator2);

        if (element1.getLocation().y < element2.getLocation().y) {
            System.out.println("The first element is above the second");
        } else {
            System.out.println("The second element is above the first");
        }

        if (element1.getLocation().x < element2.getLocation().x) {
            System.out.println("The first element is to the left of the second");
        } else {
            System.out.println("The second element is to the left of the first");
        }

        if (element1.getSize().height * element1.getSize().width > element2.getSize().height * element2.getSize().width) {
            System.out.println("The area of the first element is larger than the area of the second");
        } else {
            System.out.println("The area of the second element is larger than the area of the first");
        }
        driver.quit();
    }
}